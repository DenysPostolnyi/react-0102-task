import React from 'react';

function Header() {
    return React.createElement('h1', {style: {color: '#999', fontSize: '19px'}}, 'Solar system planets');
}

export default Header;
